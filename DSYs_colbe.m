function DSY_M=DSYs_colbe(hourly_data,lon,lat, WS_id, A)
%                        daily_data,
% The mid upper quatertile mean temperatuer during April to September

% Define variables:
%         hourly_data        -- UKCP09 weather generator hourly data
%         VP                 -- Vapour pressure 

%         Alt                -- Altitude
%         El                 -- Solar elevation 

%         T_dewpoint         -- Dew point temperature
%         RH                 -- Relative humidity (%)
%         Atmospheric_P      -- Atmospheric pressure (pasca)

%         I_hr   -- Extra-terrestrial horizontal radiance (Wh�m�2)
%         I_dr   -- Extra-terrestrial direct radiance (Wh�m�2)

%         DiffuseHorizontalR -- Diffuse horizontal radiation (W�m�2)
%         DirectNormalR      -- Direct normal radiation (W�m�2)
%         GIRad              -- Global irradiance for any given cloud amount (W�m�2)

%         L_sky   -- Horizontal Infrared Radiation from Sky (W�m�2)

%         Illuminance_g      -- Global illuminance on the horizontal plane (lux)
%         Illuminance_dh     -- Diffuse illuminance on the horizontal plane (lux)
%         Illuminance_dr     -- Direct normal illuminance (lux)
%         Luminance_z        -- Zenith luminance (cd/m2)

%         Wind_D             -- Wind direction (degree)
%         Wind_S             -- Wind speed (m/s)
%         filestem           -- Observed weather data in 2013 from MIDAS

%         ts_cover           -- Total sky cover (deka)
%         os_cover           -- Opaque sky cover
%         Visibility         -- Visibility
%         Ceiling_H          -- Ceiling height

%         Weather_obsv       -- Present weather observation
%         Weather_code       -- Present weather code

%         PW                 -- Precipitation water (mm/hour)

%         Optical_depth      -- Aerosol optical depth
%         Snow_depth         -- Snow depth
%         Last_snow_fall     -- Days since last snow fall


% DSY_Y
Alt=10;
% month0=hourly_data(:,2);
% year0=hourly_data(:,1);
% year1=daily_data(:,1);
 %   calculate mean T during April to September
% poolobj = gcp;
% addAttachedFiles(poolobj,{'SolarAzEl_liu.m','dew_point_temperature.m',...
%     'Extra_terrestrial_SolarR_DSY.m','Solar_Radiation.m','Horizontal_Infrared_Radiation.m',...
%     'Illuminance_DSY.m','WD_WS_AP_from_MIDAS_colbe_DSY.m','insertrows_liu.m'})

    for jj=1:30
        T_Apr_Sep=zeros(1);  
        for ii=4:9 
            monP2=(hourly_data(:,2)==ii) & (hourly_data(:,1)==3000+jj); 
            T_m=hourly_data(monP2,6);
            T_Apr_Sep=vertcat(T_Apr_Sep,T_m);
            % choose the year with 4th biggest Tmean            
        end
        T_Apr_Sep(1,:)=[]; 
        T_mean(jj)=nanmean(T_Apr_Sep);
 
    end
    [~,I] = sort(T_mean); 
    year=3000+I(27); monPh=hourly_data(:,1)==year; 
%     monPd=find(year1==year);

DSY_hly=hourly_data(monPh,:);
    date(:,1)=DSY_hly(:,1)-1000; % SolarAzEl_liu can only recognise year under 3000
    date(:,2)=DSY_hly(:,2);
    date(:,3)=DSY_hly(:,3);
    date(:,4)=DSY_hly(:,4)/100;
    
    [~, El]= SolarAzEl_liu(date,lat,lon,Alt);
%%
% addpath('C:\Chunde Liu 2013\MATLAB codes for PRYs\functions for missing weather variables');
% PathFunction=genpath('functions_for_missing_weather_variables');
% addpath(PathFunction);
% 1 Dew point temperature
T_dewpoint=dew_point_temperature( DSY_hly);
% 2 Sky Cover          ts_cover(data_indice),os_cover(data_indice),
sf= DSY_hly(:,10);
ts_cover=10*(1-sf);
os_cover=round(0.5*ts_cover);
% 3 Precipitation
PW= DSY_hly(:,5);
% 4 Relative Humidity
RH= DSY_hly(:,8).*100;
% 5 Extraterrestrial Solar Radiance 
[I_hr,I_dr]= Extra_terrestrial_SolarR_DSY(El, year);
% 6 Solar Radiation Calculation
[GIRad,DiffuseHorizontalR,DirectNormalR]=Solar_Radiation(sf,El);
% 7 Horizontal Infrared Radiation from Sky
L_sky= Horizontal_Infrared_Radiation( DSY_hly);
% 8 Illuminance and Zenith luminance
[Illuminance_g,Illuminance_dr,Illuminance_dh,Luminance_z]= Illuminance_DSY( DSY_hly,I_dr,...
                                 GIRad,DiffuseHorizontalR,DirectNormalR,El);
% 9 Wind Direction, Speed and Atmospheric Pressure
[Wind_D,Wind_S,Atmospheric_P]=WD_WS_AP_from_MIDAS_colbe_DSY(DSY_hly, WS_id, A);
DSY_M=zeros();

% calendar=[DSY_hly(:,1),date(:,2:4),minute];
   L=length(date(:,1));
  
   minute=zeros(L,1);
% Unavailable weather variables
   Visibility=9999+zeros(L,1);
   Ceiling_H=99999+zeros(L,1);
   Weather_obsv=9+zeros(L,1);
   Weather_code=999999999+zeros(L,1);
   Optical_depth=999+zeros(L,1);
   Snow_depth=999+zeros(L,1);
   Last_snow_fall=99+zeros(L,1);
   
  DSY_M=[DSY_hly(:,1),date(:,2:4),minute,DSY_hly(:,6),T_dewpoint,...
       RH,Atmospheric_P,I_hr,I_dr,L_sky,...
       GIRad,DirectNormalR,DiffuseHorizontalR,...
       Illuminance_g,Illuminance_dr,Illuminance_dh,...
       Luminance_z,Wind_D,Wind_S,ts_cover,os_cover,...
       Visibility,Ceiling_H,Weather_obsv,Weather_code,PW,Optical_depth,Snow_depth,Last_snow_fall];  
 
 
end

