% filename: COLBE_WF.m
% Purpose: create the 10th, 50th and 90th percentile TRYs and DSYs using
% the outputs from the Spatial Weather Generator for the UK.
% 
% 
% cd ('C:\Chunde2017\HPC_BalenaTest1')
% profile on

CurrentDir=pwd;  
addpath(genpath(CurrentDir));
Listing = dir('InputsSpatialWGdata');  
Listing([Listing.isdir])= [];
SpatialWG_folder=length(Listing)/2;  

PathFunction=genpath('functions_for_missing_weather_variables');
addpath(PathFunction);
% poolobj = gcp;
% addAttachedFiles(poolobj,{'SolarAzEl_liu.m','dew_point_temperature.m',...
%     'Extra_terrestrial_SolarR.m','Solar_Radiation.m','Horizontal_Infrared_Radiation.m',...
%     'Illuminance.m','WD_WS_AP_from_MIDAS_colbe.m','insertrows_liu.m',...
%     'Illuminance_DSY.m','WD_WS_AP_from_MIDAS_colbe_DSY.m'})
WeatherStation_Info=csvread('WS_id_lat_lon_alt.csv'); % Read weather station information
A=csvread('WS_ID.csv'); 
datasetname0='/MIDAS_2013_UK_01.csv'; WSP_data0=h5read('MIDAS_BADC.h5', datasetname0);

percentile=[10, 50, 90]; % extract only the 10th, 50th and 90th percentiles
TimePeriods = '2080s';
% GridName_list=[3951140,4951140]; 
% info = h5info('Grid.h5'); attname='UKCP09 grid number';
% info_MIDAS=h5info('MIDAS_BADC.h5'); 
formatSpec1 = '%g,%g,%g,%g,%g,%s,%2.1f,%2.1f,%g,%g,%g,%g,%g,%2.1f,%2.1f,%2.1f,%g,%g,%g,%g,%g,%2.1f,%g,%g,%g,%g,%g,%u,%2.1f,%g,%g,%g';
n=262968;

SpatialWG_folder=strsplit(Listing(1).folder, '\');
OutputsFolder=sprintf('%s%s','Outputs_',SpatialWG_folder{1,4});
mkdir(OutputsFolder);
% tic
for gg=1:length(Listing)   % number of locations (UKCP09 grids)
%  gg=1
%     datasetname=sprintf('/%s',info.Datasets(gg).Name);   %e.g. /3951140_80s  
%     data = h5read('Grid.h5', datasetname); % 26296800 x 12 double    
%     GridNo =h5readatt('Grid.h5',datasetname,attname);    
File_name=Listing(gg).name;
data=dlmread(File_name); 

number = regexp(File_name,'_','split'); GridNo=number{1,1};
    TRYs_foldername=sprintf('%s_TRYs',GridNo);
    mkdir(TRYs_foldername);
    DSYs_foldername=sprintf('%s_DSYs',GridNo);
    mkdir(DSYs_foldername);

    %GridNo is char
    [lon,lat] = GridNotoEN( GridNo ); % Calculate longitude and latitude
    % find the nearest weather station id
        arclen=zeros(125,1);
        for ii=1:125
            lat2=WeatherStation_Info(ii,2); lon2=WeatherStation_Info(ii,3); 
            arclen(ii)= distance(lat,lon,lat2,lon2);
        end
            [~,ip]= sort(arclen); 
        for bb=1:125
            WS_id_temp=WeatherStation_Info(ip(bb),1); 
            ind=find(WSP_data0(:,6)==WS_id_temp); 
             if isempty(ind)~=1 && median(WSP_data0(ind,7))>0
                 WS_id=WS_id_temp;                 
                 break
             end
        end
         
    TRY_Jan_100=zeros(1,32); TRY_Feb_100=zeros(1,32);
    TRY_Mar_100=zeros(1,32); TRY_Apr_100=zeros(1,32);
    TRY_May_100=zeros(1,32); TRY_Jun_100=zeros(1,32);
    TRY_Jul_100=zeros(1,32); TRY_Aug_100=zeros(1,32);
    TRY_Sep_100=zeros(1,32); TRY_Oct_100=zeros(1,32);
    TRY_Nov_100=zeros(1,32); TRY_Dec_100=zeros(1,32);

    AllData=cell(100,1); % initialization
    for ii=1:100    
        rows=(n*(ii-1)+1:n*ii);    
        AllData{ii,1}=data(rows, :);
    end

parfor jj=1:100

     % read hourly data  
    hourly_data=AllData{jj,1};  
%% TRY_T_S  & DSY_Y 
%TRY_T_S
   TRY_T_S=TRYs_colbe(hourly_data, lon,lat, WS_id, A);

%%   end
% toc
    % Time coverts to 24 hours convention                       
        b=TRY_T_S(:,4)/100;
        b(b==0)=24;
        TRY_T_S(:,4)=b;
        for kk=0:length(TRY_T_S)/24-1
            temp=TRY_T_S(24*kk+1,:);
            for hh=24*kk+1:24*(kk+1)-1
                TRY_T_S(hh,:)=TRY_T_S(hh+1,:);
            end
                TRY_T_S(24*(kk+1),:)=temp;
        end
      
% select reguired percentile
    % For N th percentile TRYs
    % mean monthly values
    TRY_Jan= TRY_T_S(TRY_T_S(:,2)==1,:); TRY_Jan(:,32)=jj; TRY_Jan_100=vertcat(TRY_Jan_100, TRY_Jan);
    TRY_Feb= TRY_T_S(TRY_T_S(:,2)==2,:); TRY_Feb(:,32)=jj; TRY_Feb_100=vertcat(TRY_Feb_100, TRY_Feb);
    TRY_Mar= TRY_T_S(TRY_T_S(:,2)==3,:); TRY_Mar(:,32)=jj; TRY_Mar_100=vertcat(TRY_Mar_100, TRY_Mar);
    TRY_Apr= TRY_T_S(TRY_T_S(:,2)==4,:); TRY_Apr(:,32)=jj; TRY_Apr_100=vertcat(TRY_Apr_100, TRY_Apr);
    TRY_May= TRY_T_S(TRY_T_S(:,2)==5,:); TRY_May(:,32)=jj; TRY_May_100=vertcat(TRY_May_100, TRY_May);
    TRY_Jun= TRY_T_S(TRY_T_S(:,2)==6,:); TRY_Jun(:,32)=jj; TRY_Jun_100=vertcat(TRY_Jun_100, TRY_Jun);  
    TRY_Jul= TRY_T_S(TRY_T_S(:,2)==7,:); TRY_Jul(:,32)=jj; TRY_Jul_100=vertcat(TRY_Jul_100, TRY_Jul);
    TRY_Aug= TRY_T_S(TRY_T_S(:,2)==8,:); TRY_Aug(:,32)=jj; TRY_Aug_100=vertcat(TRY_Aug_100, TRY_Aug);
    TRY_Sep= TRY_T_S(TRY_T_S(:,2)==9,:); TRY_Sep(:,32)=jj; TRY_Sep_100=vertcat(TRY_Sep_100, TRY_Sep);
    TRY_Oct= TRY_T_S(TRY_T_S(:,2)==10,:); TRY_Oct(:,32)=jj; TRY_Oct_100=vertcat(TRY_Oct_100, TRY_Oct);
    TRY_Nov= TRY_T_S(TRY_T_S(:,2)==11,:); TRY_Nov(:,32)=jj; TRY_Nov_100=vertcat(TRY_Nov_100, TRY_Nov);
    TRY_Dec= TRY_T_S(TRY_T_S(:,2)==12,:); TRY_Dec(:,32)=jj; TRY_Dec_100=vertcat(TRY_Dec_100, TRY_Dec);   
% DSY_Y
    DSY_M=DSYs_colbe(hourly_data, lon,lat, WS_id, A);   
     % Time coverts to 24 hours convention           
       b=DSY_M(:,4);   b(b==0)=24;   DSY_M(:,4)=b;    

        for kk=0:length(DSY_M)/24-1
            temp=DSY_M(24*kk+1,:);
            for hh=24*kk+1:24*(kk+1)-1
                DSY_M(hh,:)=DSY_M(hh+1,:);
            end
                DSY_M(24*(kk+1),:)=temp;
        end
        if length(DSY_M)>8760
            DSY_M(1417:1440,:)=[]; % delete 29 FEB
        end
       DSY_Y{jj,1}=DSY_M; 
end % loop for jj is ended here

%% TRYs.epw
    TRY_Jan_100(1,:)=[]; TRY_Feb_100(1,:)=[];
    TRY_Mar_100(1,:)=[]; TRY_Apr_100(1,:)=[];
    TRY_May_100(1,:)=[]; TRY_Jun_100(1,:)=[];
    TRY_Jul_100(1,:)=[]; TRY_Aug_100(1,:)=[];
    TRY_Sep_100(1,:)=[]; TRY_Oct_100(1,:)=[];
    TRY_Nov_100(1,:)=[]; TRY_Dec_100(1,:)=[];

% For N th percentile TRYs
num1=zeros(1,100); num2=zeros(1,100); num3=zeros(1,100);
num4=zeros(1,100); num5=zeros(1,100); num6=zeros(1,100);
num7=zeros(1,100); num8=zeros(1,100); num9=zeros(1,100);
num10=zeros(1,100);num11=zeros(1,100);num12=zeros(1,100);

    for ss=1:100 % 1:100
        T_Jan_1=mean(TRY_Jan_100(TRY_Jan_100(:,32)==ss,6));
        T_Feb_1=mean(TRY_Feb_100(TRY_Feb_100(:,32)==ss,6));
        T_Mar_1=mean(TRY_Mar_100(TRY_Mar_100(:,32)==ss,6));
        T_Apr_1=mean(TRY_Apr_100(TRY_Apr_100(:,32)==ss,6));
        T_May_1=mean(TRY_May_100(TRY_May_100(:,32)==ss,6));
        T_Jun_1=mean(TRY_Jun_100(TRY_Jun_100(:,32)==ss,6));    
        T_Jul_1=mean(TRY_Jul_100(TRY_Jul_100(:,32)==ss,6));
        T_Aug_1=mean(TRY_Aug_100(TRY_Aug_100(:,32)==ss,6));
        T_Sep_1=mean(TRY_Sep_100(TRY_Sep_100(:,32)==ss,6));    
        T_Oct_1=mean(TRY_Oct_100(TRY_Oct_100(:,32)==ss,6));
        T_Nov_1=mean(TRY_Nov_100(TRY_Nov_100(:,32)==ss,6));
        T_Dec_1=mean(TRY_Dec_100(TRY_Dec_100(:,32)==ss,6));

        num1(ss)=T_Jan_1;  num2(ss)=T_Feb_1;  num3(ss)=T_Mar_1;
        num4(ss)=T_Apr_1;  num5(ss)=T_May_1;  num6(ss)=T_Jun_1;
        num7(ss)=T_Jul_1;  num8(ss)=T_Aug_1;  num9(ss)=T_Sep_1;
        num10(ss)=T_Oct_1; num11(ss)=T_Nov_1; num12(ss)=T_Dec_1;
    end
        [~, index1] = sort(num1);  [~, index2] = sort(num2);  [~, index3] = sort(num3);
        [~, index4] = sort(num4);  [~, index5] = sort(num5);  [~, index6] = sort(num6);
        [~, index7] = sort(num7);  [~, index8] = sort(num8);  [~, index9] = sort(num9);
        [~, index10] = sort(num10);[~, index11] = sort(num11);[~, index12] = sort(num12);

% TRY_Jan_100_1=zeros(1,32); TRY_Feb_100_1=zeros(1,32); TRY_Mar_100_1=zeros(1,32);  
    for tt=1:3 
        temp1=TRY_Jan_100(TRY_Jan_100(:,32)==index1(percentile(tt)),:);
        temp2=TRY_Feb_100(TRY_Feb_100(:,32)==index2(percentile(tt)),:);
        temp3=TRY_Mar_100(TRY_Mar_100(:,32)==index3(percentile(tt)),:);

        temp4=TRY_Apr_100(TRY_Apr_100(:,32)==index4(percentile(tt)),:);
        temp5=TRY_May_100(TRY_May_100(:,32)==index5(percentile(tt)),:);
        temp6=TRY_Jun_100(TRY_Jun_100(:,32)==index6(percentile(tt)),:);

        temp7=TRY_Jul_100(TRY_Jul_100(:,32)==index7(percentile(tt)),:);
        temp8=TRY_Aug_100(TRY_Aug_100(:,32)==index8(percentile(tt)),:);
        temp9=TRY_Sep_100(TRY_Sep_100(:,32)==index9(percentile(tt)),:);

        temp10=TRY_Oct_100(TRY_Oct_100(:,32)==index10(percentile(tt)),:);
        temp11=TRY_Nov_100(TRY_Nov_100(:,32)==index11(percentile(tt)),:);
        temp12=TRY_Dec_100(TRY_Dec_100(:,32)==index12(percentile(tt)),:);    

        TRY_percentile=[temp1;temp2;temp3;temp4;temp5;temp6;temp7;temp8;temp9;temp10;temp11;temp12];
        TRY_percentile(:,32)=[];
% Header lines
header_TRY=Headerlines_EPW(TRY_percentile, GridNo, WS_id ,lat,lon);
    % Insert Uncertainty   
    Year= sscanf(TimePeriods, '%d');
    TRY_T_S_p=TRY_percentile;
    TRY_T_S_p(:,1)=Year;    % Change future year

    WeatherData_TRY{1,1}=TRY_T_S_p(:,1:5);
    S_TRY=repmat('*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?',[length(TRY_T_S_p(:,1)),1]);
    WeatherData_TRY{2,1}=S_TRY;
    WeatherData_TRY{3,1}=TRY_T_S_p(:,6:31);
    [cellrows_TRY,~] = size(WeatherData_TRY{1,1});
    filename_TRY=sprintf('%dth_%s_%s_TRY.epw',percentile(tt),TimePeriods,GridNo);
          
            fid2 = fopen(filename_TRY,'wt');       
                     for j=1:8
                         fprintf(fid2,'%s',header_TRY{j,:});
                         fprintf(fid2,'\r\n');
                     end
                     for crow = 1:cellrows_TRY
                         fprintf(fid2,formatSpec1,WeatherData_TRY{1,1}(crow,:),...
                             WeatherData_TRY{2,1}(crow,:),WeatherData_TRY{3,1}(crow,:));
                         fprintf(fid2,'\r\n');
                     end
             fclose(fid2);
        movefile(filename_TRY,TRYs_foldername); 
    end
    
%% DSYs.epw
    % extract calendar month from DSY_Y and rank month based on mean air Temp
    month_all=[]; Month_12=[];
    for mm=1:12
    %     mm=1    
        for rr=1:100 % 1:100     
    %         rr=1
            month_index=find(DSY_Y{rr,1}(:,2)==mm);        
            month_all{rr,1}=DSY_Y{rr,1}(month_index,:); % use subcell later on
        end    
       % order the months based on the mean temperature
        month_T=zeros;    
        for xx=1:100 % 1:100
            month_T(xx, 1)=mean(month_all{xx, 1}(:,6));       
        end
            [~, iy]=sort(month_T);        
            Month_12{1,mm}=month_all(iy,1); 
    end
% ASSEMBLE 12 months into a year by using vertcat function
% percentile=[10, 50, 90]; 
    for pp=1:3 % 1:100
            DSY=zeros(1,31);
            for nn=1:12
                DSY=vertcat(DSY, Month_12{1, nn}{percentile(pp), 1});
            end
            DSY(1,:)=[];  DSY(:,1)=2080;
                    
 % Header lines
    header_DSY=Headerlines_EPW(DSY, GridNo, WS_id ,lat,lon); 
    
    WeatherData_DSY{1,1}=DSY(:,1:5);
    S_DSY=repmat('*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?',[length(DSY),1]);
    WeatherData_DSY{2,1}=S_DSY;
    WeatherData_DSY{3,1}=DSY(:,6:31);

    % Save CIBSE DSY into folder
    [cellrows_DSY,~] = size(WeatherData_DSY{1,1});
    filename_DSY=sprintf('%dth_%s_%s_DSY.epw',percentile(pp),TimePeriods,GridNo);
%     filename_TRY=sprintf('%dth_%s_%s_TRY.epw',percentile(tt),TimePeriods,GridNo);          

            fid4 = fopen(filename_DSY,'wt');       
                     for j=1:8
                         fprintf(fid4,'%s',header_DSY{j,:});
                         fprintf(fid4,'\r\n');
                     end
                     for crow = 1:cellrows_DSY
                         fprintf(fid4,formatSpec1,WeatherData_DSY{1,1}(crow,:),...
                             WeatherData_DSY{2,1}(crow,:),WeatherData_DSY{3,1}(crow,:));
                         fprintf(fid4,'\r\n');
                     end
                     fclose(fid4);
                     
    movefile(filename_DSY, DSYs_foldername);   
    
    end
%     save('TRY_T_S_DSY_Y','TRY_T_S', 'DSY_Y');

 Zip_GridNo=sprintf('%s_2080s.zip', GridNo);
 zippedfiles = zip(Zip_GridNo,{TRYs_foldername,DSYs_foldername});
 movefile (Zip_GridNo, OutputsFolder)
 %Delete 100 sets of csv files, TRYs and DSYs
 rmdir(TRYs_foldername, 's'); 
 rmdir(DSYs_foldername, 's'); 
end %  loop for gg is ended here
% save COLBE_WF2_parfor.mat
% profile viewer
% profsave(profile('info'),'Profile_results')

%  toc
 
 
