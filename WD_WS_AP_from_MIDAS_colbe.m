function [WD,WS, AP]=WD_WS_AP_from_MIDAS_colbe(hourly_data,WS_id,A)
% cd ('C:\Chunde2017\01 COLBE Project\MATLAB_WF')
% CurrentDir=pwd;  addpath(genpath(CurrentDir))
% WeatherStation_Info=csvread('WS_id_lat_lon_alt.csv'); % Read weather station information
% hourly_data=csvread('r_0001_hly.csv'); lat=60.1230; long=-2.1350;
WS_hourly = [hourly_data(:,1:4), hourly_data(:,9)]; 

    % Input 2013 observed weather data 
    datasetname='/MIDAS_2013_UK_01.csv';
    WSP_data=h5read('MIDAS_BADC.h5', datasetname);
    ind=find(WSP_data(:,6)==WS_id);
WSP_data1=[WSP_data(ind,1:7),WSP_data(ind,8).*0.515,100*WSP_data(ind,9)];

for rr=2:12
    datasetname=sprintf('/MIDAS_2013_UK_%02d.csv',rr);
    WSP_data=h5read('MIDAS_BADC.h5', datasetname);    
    ind1=find(WSP_data(:,6)==WS_id);
    Temp1=[WSP_data(ind1,1:7),WSP_data(ind1,8).*0.515,100*WSP_data(ind1,9)];
    WSP_data1=vertcat(WSP_data1,Temp1);    
end

[~, ia, ~] = unique(WSP_data1(:,1:4),'rows'); % remove the repeated rows
[~, index1] = sort(ia);
ia = ia(index1);

% A=csvread('WS_ID.csv');  
B=WSP_data1(ia,:); [Brows, ~]=size(B);

%% to create a complete observed year
if length(B(:,1))~=8760
    A1=A(:,1:5);   B1=B(:,1:5);   B2=B(:,6:9); 
    [~,ih] = setdiff(A1,B1,'rows');
k = [true;diff(ih(:))~=1 ];
% s = cumsum(k); 
[n,~]=histcounts(cumsum(k),unique(cumsum(k)));
mat1=B1;

    for tt=1:length(n)
        if tt==1
            r=ih(tt);          
            mat2=A1(r:r-1+n(tt),:);  
            new1=insertrows_liu(mat1,mat2,r); % time
            if r<7                
                mat3=repmat(mean(B2(r:r+7,:)),[n(tt),1]);
            else
                mat3=repmat(mean(B2(r-7:r-1,:)),[n(tt),1]);              
            end
            new2=insertrows_liu(B2,mat3,r);   % weather variables
        else
            if n(tt)~=1
                m=sum(n(1:tt-1))+1;
                r=ih(m);
            else
                m=sum(n(1:tt));
                r=ih(m); 
            end            
            mat2=A1(r:r-1+n(tt),:);
            
            if r>Brows
                mat3=repmat(mean(B2(Brows-10:Brows-1,:)),[n(tt),1]);
            else
                mat3=repmat(mean(B2(r-7:r-1,:)),[n(tt),1]);
            end        
            new1=insertrows_liu(new1,mat2,r);
            new2=insertrows_liu(new2,mat3,r);        
        end
    end
    WSP_data_new=[new1,new2];
else
    WSP_data_new=B;
end
WD_obs=WSP_data_new(:,7);   % hourly wind direction
month=WSP_data_new(:,2);
day=WSP_data_new(:,3);
hour=WSP_data_new(:,4);
% month_d=[mean(reshape(month, 24, []))',mean(reshape(day, 24, []))'];

t1=datetime('01/01/2013','InputFormat','dd/MM/yy');
t2=datetime('31/12/2013','InputFormat','dd/MM/yy');
YearTime=datevec((t1:t2)'); 
month_d=YearTime(:,2:3);

WS_obs=[month_d,mean(reshape(WSP_data_new(:,8),24,[]))'];   % daily wind speed

% obtain Atmospheric Pressure from observed data
Atmospheric_P=WSP_data_new(:,9);
ind3 = find(Atmospheric_P, 720, 'first');
ind4 = find(Atmospheric_P, 720, 'last');
Atmospheric_P(Atmospheric_P==0)=(mean(Atmospheric_P(ind3))+mean(Atmospheric_P(ind4)))/2;

if isnan(Atmospheric_P)==1
    Atmospheric_P(:)=101325;
end

%% Compare hourly wind speed between the generated and observed ones within
% the same calendar month  : WS_hourly vs WS_obs
% 7 leap years(3001-3030): 3004,3008,3012,3016,3020,3024,3028
% [WD_hourly,WS_hourly, AP,
    WD=0; AP=0;

    for yy = 1:30
    %     yy=1
   
        WS_h=WS_hourly(WS_hourly(:,1)==3000+yy,5);
        if leapyear(3000+yy)==1
            m_d=insertrows_liu(month_d, [2, 29], 60);
            WS_temp_0=reshape(WS_h,24,[]);
            WS_30d = [m_d, mean(WS_temp_0)']; % from spatial Weather Generator
        else
            m_d=month_d;
    %         WS_30d{yy} = [m_d, mean(reshape(WS_h,24,[]))']; 
            WS_temp_0=reshape(WS_h,24,[]);
            WS_30d = [m_d, mean(WS_temp_0)']; 
        end    
        ip=[]; pp=1;
        for mm = 1:12
    %         mm=8  
   
            [ind_M,~]=find(WS_30d(:, 1)==mm); 
            [ind_m,~]=find(WS_obs(:, 1)==mm); % from observation
            for DD=1:length(ind_M)
                WS_temp1=repmat(WS_30d(ind_M(DD),3),[length(ind_m),1]); 
                [~,ip(pp)] = min(abs(WS_temp1-WS_obs(ind_m,3)));
                pp=pp+1; 
            end
            ip(ind_M)=ip(ind_M)+ind_M(1)-1;
        end
    %      ax=[WS_30d,  WS_obs(ip,3)];
       ipp1=0;
       for qq = 1:length(ip)
           ipp0 = ((ip(qq)-1)*24+1:ip(qq)*24)';
           ipp1=vertcat(ipp1, ipp0);       
       end
       ipp1(1)=[]; % index for extracting WD and Atmospheric_P from WSP_data_new (obs)
       if leapyear(3000+yy)==1
           WD_obs0=insertrows_liu(WD_obs, WD_obs(1393:1416), 1417);
           Atmospheric_P0=insertrows_liu(Atmospheric_P, Atmospheric_P(1393:1416), 1417);
       else
           WD_obs0=WD_obs;   Atmospheric_P0=Atmospheric_P;
       end
    %    WD_hourly0{1,yy}=WD_obs0(ipp1);   AP_hourly0{1,yy}= Atmospheric_P0(ipp1); 
       WD_hourly0=WD_obs0(ipp1);   AP_hourly0= Atmospheric_P0(ipp1); 

       WD=vertcat(WD,WD_hourly0); 
       AP=vertcat(AP,AP_hourly0);
    end  
   WD(1)=[]; WD=round(WD);
   AP(1)=[]; AP=round(AP);
   WS= hourly_data(:,9); 
end % function end