# Method for plotting UK overheating

To use, clone this repository:

https://bitbucket.org/mjwood/mapping-overheating/

Run "makemap.py" you should get the plot. Currently this just takes in a .csv file (WS_id_lat_lon_alt.csv) and plots the location and  altitude of the weather stations. This can easily be generalised to produce overheating maps.

