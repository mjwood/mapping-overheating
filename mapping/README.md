# Chunde's request

I am glad that you can have a look into it. I would like to produce a map with a grid resolution of 5km by 5km like the attached one. The grid map information downloaded from UKCP09 user interface are attached.

I plan to produce weather files (.zip) across the UK (approximately 11400 grid locations). So far, I have produced them for about 60% of the UK. The sample of weather files are attached. Regarding the name of weather files, 4201070 is the grid number while 2080s means future time slice.​ I will carry out multiple building simulations with these weather files to predict the risk of overheating (for instance, hours above indoor temperature of 26°C) across the UK. I would  like to map the risk of overheating at a 5km by 5km grid resolution.

If you have an idea of mapping, for instance, the risk of overheating at a 5km by 5km grid resolution, that would be very helpful.
