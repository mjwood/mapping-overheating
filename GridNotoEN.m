function [long,lat] = GridNotoEN( GridNo )
% create long and lat from UKCP09 grid square

%Set the location of the '5km_grid_box_ids.csv' 
data = importdata('5km_grid_box_ids.csv');
for i = 1:1:290
    for j = 1:1:180
        if str2double(GridNo) == data(i, j)
           easting = data(1, j);
           northing = data(i, 1);
          
           %'once found leave the loop
        end
    end
end

N = northing;
E = easting;
a = 6377563.396;
b = 6356256.91;              %'// Airy 1830 major & minor semi-axes
F0 = 0.9996012717;            %'// NatGrid scale factor on central meridian
lat0 = 49 * pi / 180;
lon0 = -2 * pi / 180;         %'// NatGrid true origin
N0 = -100000;
e0 = 400000;                %'// northing & easting of true origin, metres
e2 = 1 - (b * b) / (a * a);   %'// eccentricity squared
n1 = (a - b) / (a + b);
n2 = n1 * n1;
n3 = n1 * n1 * n1;
lat = lat0;
M = 0;
while (N - N0 - M >= 0.00001) %'// ie until < 0.01mm
  lat = (N - N0 - M) / (a * F0) + lat;
  Ma = (1 + n1 + (5 / 4) * n2 + (5 / 4) * n3) * (lat - lat0);
  Mb = (3 * n1 + 3 * n1 * n1 + (21 / 8) * n3) * sin(lat - lat0) * cos(lat + lat0);
  Mc = ((15 / 8) * n2 + (15 / 8) * n3) * sin(2 * (lat - lat0)) * cos(2 * (lat + lat0));
  Md = (35 / 24) * n3 * sin(3 * (lat - lat0)) * cos(3 * (lat + lat0));
  M = b * F0 * (Ma - Mb + Mc - Md);                %'// meridional arc
end
cosLat = cos(lat);
sinLat = sin(lat);
nu = a * F0 / (1 - e2 * sinLat * sinLat) ^ 0.5;  %'// transverse radius of curvature
rho = a * F0 * (1 - e2) * (1 - e2 * sinLat * sinLat) ^ (-1.5); %'// meridional radius of curvature
eta2 = nu / rho - 1;

tanLat = tan(lat);
tan2Lat = tanLat * tanLat;
tan4Lat = tan2Lat * tan2Lat;
tan6Lat = tan4Lat * tan2Lat;
secLat = 1 / cosLat;
nu3 = nu * nu * nu;
nu5 = nu3 * nu * nu;
nu7 = nu5 * nu * nu;

VII = tanLat / (2 * rho * nu);
VIII = tanLat / (24 * rho * nu3) * (5 + 3 * tan2Lat + eta2 - 9 * tan2Lat * eta2);
IX = tanLat / (720 * rho * nu5) * (61 + 90 * tan2Lat + 45 * tan4Lat);
x = secLat / nu;
XI = secLat / (6 * nu3) * (nu / rho + 2 * tan2Lat);
XII = secLat / (120 * nu5) * (5 + 28 * tan2Lat + 24 * tan4Lat);
XIIA = secLat / (5040 * nu7) * (61 + 662 * tan2Lat + 1320 * tan4Lat + 720 * tan6Lat);

dE = (E - e0);
DE2 = dE * dE;
dE3 = DE2 * dE;
dE4 = DE2 * DE2;
dE5 = dE3 * DE2;
dE6 = dE4 * DE2;
dE7 = dE5 * DE2;
lat = lat - VII * DE2 + VIII * dE4 - IX * dE6;
lon = lon0 + x * dE - XI * dE3 + XII * dE5 - XIIA * dE7;
lat = round(1000 * (lat * 180 / pi)) / 1000;
long = round(1000 * lon * 180 / pi) / 1000;

end

