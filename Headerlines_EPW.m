function header=Headerlines_EPW(inputs, GridNo, WS_id ,lat,lon)
% for TRY, inputs = TRY_T_S' ;
% for DSY, inputs = DSY;

    Time_Zone=0; Elevation=20;
    Ground_T=Ground_temperature(inputs);
    

    header{1,1}='LOCATION,';   
    header{1,2}=GridNo;
    header{1,3}=sprintf(',-,UK,WG data,%.5d,%2.2f,%2.2f,%d,%d', WS_id ,lat,lon,Time_Zone,Elevation);

    header{2,1}='DESIGN CONDITIONS,0';
    header{3,1}='TYPICAL/EXTREME PERIODS,0';

    header{4,1}='GROUND TEMPERATURES,1,1.0,,,';
    header{4,2}=sprintf(',%2.1f',Ground_T);

    header{5,1}='HOLIDAYS/DAYLIGHT SAVINGS,No,0,0,0';
    header{6,1}='COMMENTS 1,created by using outputs from UK Spatial Weather Generator';
    header{7,1}=sprintf('COMMENTS 2, wind direction and atmospheric pressure based on the nearest observations available at station id %.5d', WS_id);
    header{8,1}='DATA PERIODS,1,1,Data,Sunday,1/1,12/31';





end





