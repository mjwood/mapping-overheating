function [Gt,DiffuseHorizontalR,DirectNormalR]= Solar_Radiation(sf,El)
% Purpose:
%     To calculate global solar radiation on the hourly scale by 
%     using "cloud cover data" as the key input. 
%     This calculated hourly solar radiation using Cloud-Cover Radiation 
%     Model will swap the one produced by UKCP09 Weather Generator.
%
% Define variables: 
%         cc      -- Cloud cover_fraction
%         El      -- Solar altitude 
%         Gt_0    -- Global irradiance under a corresponding cloudless sky
%         Gt      -- Global irradiance for any given cloud amount   
%         
%         sf      -- Sunshine fraction     
%         A,B,C,D -- Locally fitted coefficients   
% DiffuseHorizontalR -- Diffuse horizontal radiation (W�m�2)
% DirectNormalR   -- Direct normal radiation (W�m�2)

%        ts_cover --  Total sky cover
%        os_cover --  Opaque sky cover


Gt_0=948*sind(El)-49;
cc=1-sf;                
Gt=(1-0.71*(cc.^3.4)).*Gt_0;

Gt(Gt<0) = 0;
DiffuseHorizontalR=(0.3+0.7*(cc.^2)).*Gt;
DirectNormalR=(Gt-DiffuseHorizontalR)./sind(El);

end

















