function [ WS ] = Wind_speed_from_PET( lat,daily_data )
    %WIND_SPEED_FROM_PET Summary of this function goes here
    %   Detailed explanation goes here
    calc_wind_sp = zeros(size(daily_data,1),2);
    calc_wind_sp(:,1) = 1:size(daily_data,1);
    calc_wind_sp2 = calc_wind_sp;
    threshold = 30;
    
    %convert wind speed between 2 and 10 meters.
    wnf = log((2 - 0.08) / 0.015);
    wnf = wnf / log((10 - 0.08) / 0.015);
    
    avp = 0.1 * daily_data(:,9);
    latr = lat * (pi / 180);
    slrd = 0.409 * sin((0.0172 * (daily_data(:,4))) - 1.39); %' jd
    snst = (-tan(latr) .* tan(slrd));
    snst = 2 * atan((1 - snst .^ 2) .^ 0.5 ./ (1 + snst));
    dr = 1 + 0.033 * cos(0.0172 * (daily_data(:,4)));  %' jd
    dter = 37.6 * dr .* (snst .* sin(latr) .* sin(slrd) + cos(latr) .* cos(slrd) .* sin(snst));
    tm = (daily_data(:,7) + daily_data(:,8)) / 2; % ' tmin + tmax
    vptm = 0.1 * (tm);
    dvp = (4099 * vptm) ./ (tm + 237.3) .^ 2;
    tmy = tm;
    tmy(2:end) = tm(1:end-1);
    tflux = 0.38 * (tm - tmy);
    psy = 0.067;
    dyln = 7.64 * snst;
    SS = dyln;
    SS(daily_data(:,11) < dyln) = daily_data(daily_data(:,11) < dyln,11);
    cl = 1 - SS ./ dyln;
    ssf = 1 - cl;
    ktn = daily_data(:,7) + 273.16; %' tmin
    ktx = daily_data(:,8) + 273.16; %' tmax
    rns = 0.77 * (0.25 + (0.5 * (ssf))) .* dter;
    rnl = 0.00000000245 * (0.9 * (ssf) + 0.1) .* (0.34 - 0.14 * (avp) .^ 0.5) .* (ktn .^ 4 + ktx .^ 4);
    rnet = rns - rnl;
    vptn = 0.1 * (daily_data(:,7)); %' tmin
    vptx = 0.1 * (daily_data(:,8)); %' tmax
    vpd = ((vptx + vptn) / 2) - avp;
    a = 0.408 * dvp .* (rnet - tflux);
    pet = daily_data(:,14);  %'pet
    %if ((900 ./ (tm + 273.16)) * psy .* vpd - 0.34 * psy .* pet) ~= 0
    wn2m = (pet .* dvp + pet * psy - a) ./ ((900 ./ (tm + 273.16)) * psy .* vpd - 0.34 * psy .* pet);
    calc_wind_sp(:,2) = wn2m / wnf;
    
calc_wind_sp((900 ./ (tm + 273.16)) * psy .* vpd - 0.34 * psy .* pet == 0,2) = -999;

%     calc_wind_sp(calc_wind_sp((900 ./ (tm + 273.16)) * psy .* vpd - 0.34 * psy .* pet == 0,2),2) = -999;
    
    calc_wind_sp(calc_wind_sp(:,2) < 0,2) = -999;
    
    b = psy + dvp;
    C = 0.34 * psy;
    D = 900 ./ (tm + 273.16) * psy .* vpd;
    diff = (b .* D - a .* C) ./ (C .* daily_data(:,14) - D) .^ 2;
    calc_wind_sp(calc_wind_sp(:,2) > threshold,2) = -999;
    calc_wind_sp(abs(diff) > 1000,2) = -999;
    
    calc_wind_sp(calc_wind_sp(:,2) == -999,:) = [];
    
    WS = interp1(calc_wind_sp(:,1),calc_wind_sp(:,2),1:size(daily_data,1)')';
    %'If calc_wind_sp > threshold Then
    %'   calc_wind_sp = threshold
    %'End If
    %WS = calc_wind_sp;
    
end

