function L_sky= Horizontal_Infrared_Radiation(hourly_data)
% Purpose: 
%        To create Horizontal Infrared Radiation from Sky
% 
% 
% Define variables:
%            Ta      -- Ambient temperature (K)
%            Pw      -- Vapour pressure     (hPa)
%            sf      -- Sunshine fraction
%            L_sky   -- Horizontal Infrared Radiation from Sky (W/m2)
%            a       -- UKCP09 weather generator hourly data
%            SB      -- Stefan-Boltzmann constant

SB_constant=5.6697*10^(-8);
Ta=hourly_data(:,6)+273.15;
Pw=hourly_data(:,7);
sf=hourly_data(:,9);

L_sky=round(SB_constant*(Ta.^4).*[0.904-(0.304-0.061*Pw.^0.5).*sf-0.005*Pw.^0.5]);

