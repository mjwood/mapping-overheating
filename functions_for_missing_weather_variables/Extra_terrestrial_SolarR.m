function [I_hr,I_dr]= Extra_terrestrial_SolarR(El)
% Purpose: 
%        To calculate Extra-terrestrial horizontal radiance(Wh�m�2) and 
%        Extra-terrestrial direct radiance(Wh�m�2) according to the CIBSE
%        Guide J pp.87
%
%        Add an alternative method of Extra-terrestrial direct radiance
%        calculation in order to match the values given by on Matt's 
%
%
% Define variables: 

%         I_hr   -- Extra-terrestrial horizontal radiance (Wh�m�2)
%         I_dr   -- Extra-terrestrial direct normal radiance (Wh�m�2)
%         Lat    -- Latitude of the location (degree)
%         DA     -- Day angle (radian)
%         Ws     -- Sunset hour angle (degree)
%         SD     -- Solar declination (degree)
%         Ej     -- Correction factor for variation in solar distance
%
DA=zeros(1,1);

for ii=3001:3030
    for jj=1:yeardays(ii)
        temp1(jj,1)=jj*(360/365.25); % Day angle (degree)
    end
        DA=vertcat(DA, temp1);
        temp1=[]; 
end
DA(1)=[];

DA_hourly=kron(DA, ones(24,1)); % repeat daily value 24 times
Ej_hourly=1+0.03344*cosd(DA_hourly-2.8); % Correction factor

% Extra-terrestrial horizontal radiance (Wh�m�2)
I_hr=round(1367*Ej_hourly.*sind(El)); 
I_hr(I_hr<0)=0;

I_dr=round(I_hr./sind(El));






