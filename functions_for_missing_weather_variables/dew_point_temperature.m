function T_dewpoint=dew_point_temperature(hourly_data)
% Purpose: 
%        To calculate dew point temperature from UKCP09 WG outputs 

% Define variables:
%          VP           -- Vapour pressure 
%          T_dewpoint   -- Dew point temperature
%          jj           -- Index    
%          alpha        -- lnVP 

VP=hourly_data(:,7)/10;

% T_dewpoint=zeros(262968,1);

for jj=1:length(VP)
    alpha=log(VP(jj));   
if VP(jj)>0.61115
    T_dewpoint1(jj)=6.54+14.526*alpha+0.7389*alpha^2+0.09486*alpha^3+0.4569*VP(jj)^0.1984;
elseif VP(jj)<0.61115&&VP(jj)~=0
    T_dewpoint1(jj)=6.09+12.608*alpha+0.4959*alpha^2;
else 
%     VP(jj)=0.01;
    T_dewpoint1(jj)=6.09+12.608*log(0.01)+0.4959*log(0.01)^2;
end
end
T_dewpoint=round(T_dewpoint1'.*10)/10;



