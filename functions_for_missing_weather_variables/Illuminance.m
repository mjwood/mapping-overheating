function [Illuminance_g,Illuminance_dr,Illuminance_dh,Luminance_z]= Illuminance(hourly_data,I_dr,Gt,Df,Dr,El)
% Purpose:
%     To calculate illuminance (global, diffuse, direct) according to Perez
%     model
%
% Define variables: 
%         Illuminance_g      -- Global illuminance on the horizontal plane (lux)
%         Illuminance_dh     -- Diffuse illuminance on the horizontal plane (lux)
%         Illuminance_dr     -- Direct normal illuminance (lux)
%         Luminance_z        -- Zenith luminance (cd/m2)

%         I_hr   -- Extra-terrestrial horizontal radiance (Wh�m�2)
%         I_dr   -- Extra-terrestrial direct radiance (Wh�m�2)

%         Gt     -- Global horizontal irradiance on the horizontal plane (W�m�2)
%         Df     -- Diffuse horizontal irradiance on the horizontal plane (W�m�2)
%         Dr     -- Dirrect normal irradiance (W�m�2)

%         W      -- Atmospheric precipitable water content (cm)

%         Lat    -- Latitude of the location (degree)

%         e,e1,pp-- Sky's clearness
%         E      -- Sky's clearness bin value (1~8)
%         delta  -- Sky's brightness
%         m      -- Relative optical airmass

%         co,p   -- Luminous efficacy and zenith luminance model coefficients
%         a,b,c,d-- eight term vectors corresponding to eight e bins

%         El     -- Solar altitude (degree) 
%         Az     -- Solar azimuth (degree)
%         z      -- Zenith angle (degree)
%         DA     -- Day angle (radian)
%         Ws     -- Sunset hour angle (degree)
%         SD     -- Solar declination (degree)
%         Ej     -- Correction factor for variation in solar distance
%         jd     -- Julian date


precip_htotal=hourly_data(:,5);

W=precip_htotal/10; % Unit conversion: mm to cm

m=(sind(El)+0.15*(El+3.885).^(-1.253)).^(-1);
delta=(Df.*m)./I_dr;

z=degtorad(90-El);
e1=((Df+Dr)./Df + 1.041*z.^3).*((1+1.041*z.^3).^(-1));
e=single(e1);

co_g=zeros(262968,4);
co_dr=zeros(262968,4);
co_dh=zeros(262968,4);
co_zl=zeros(262968,4);
for ii=1:length(e)
    if isnan(e(ii))==1
    e(ii)=0;
    end
end

co1=load('coefficient_Perez.mat');
co=co1(1).co(1:8,1:17);  


for ii=1:length(e)
if e(ii)>=1&&e(ii)<1.065
%     E(ii,1)=1;
    co_g(ii,:)=co(1,2:5); 
    co_dr(ii,:)=co(1,6:9); co_dh(ii,:)=co(1,10:13); co_zl(ii,:)=co(1,14:17);
    elseif e(ii)>=1.065&&e(ii)<1.203
%     E(ii,1)=2;
    co_g(ii,:)=co(2,2:5);
    co_dr(ii,:)=co(2,6:9); co_dh(ii,:)=co(2,10:13); co_zl(ii,:)=co(2,14:17);
    elseif e(ii)>=1.203&&e(ii)<1.5
%     E(ii,1)=3;
    co_g(ii,:)=co(3,2:5);
    co_dr(ii,:)=co(3,6:9); co_dh(ii,:)=co(3,10:13); co_zl(ii,:)=co(3,14:17);
    elseif e(ii)>=1.5&&e(ii)<1.95
%     E(ii,1)=4;
    co_g(ii,:)=co(4,2:5);
    co_dr(ii,:)=co(4,6:9); co_dh(ii,:)=co(4,10:13); co_zl(ii,:)=co(4,14:17);
    elseif e(ii)>=1.95&&e(ii)<2.8
%     E(ii,1)=5;
    co_g(ii,:)=co(5,2:5);
    co_dr(ii,:)=co(5,6:9); co_dh(ii,:)=co(5,10:13); co_zl(ii,:)=co(5,14:17);
    elseif e(ii)>=2.8&&e(ii)<4.5
%     E(ii,1)=6;
    co_g(ii,:)=co(6,2:5);
    co_dr(ii,:)=co(6,6:9); co_dh(ii,:)=co(6,10:13); co_zl(ii,:)=co(6,14:17);
    elseif e(ii)>=4.5&&e(ii)<6.2
%     E(ii,1)=7;
    co_g(ii,:)=co(7,2:5);
    co_dr(ii,:)=co(7,6:9); co_dh(ii,:)=co(7,10:13); co_zl(ii,:)=co(7,14:17);
    elseif e(ii)>6.2
%     E(ii,1)=8;
    co_g(ii,:)=co(8,2:5);
    co_dr(ii,:)=co(8,6:9); co_dh(ii,:)=co(8,10:13); co_zl(ii,:)=co(8,14:17);
    elseif isnan(e(ii))==1
%     E(ii,1)=0;
    co_g(ii,:)=0;
    co_dr(ii,:)=0; co_dh(ii,:)=0; co_zl(ii,:)=0;
    end
end


% Illuminance calculation
Illuminance_g=zeros(); Illuminance_dr=zeros(); Illuminance_dh=zeros(); Luminance_z=zeros(); 

Illuminance_g=round(Gt.*(co_g(:,1)+co_g(:,2).*W+co_g(:,3).*cos(z)+co_g(:,4).*log(delta))); 

Illuminance_dr=round(Dr.*(co_dr(:,1)+co_dr(:,2).*W+co_dr(:,3).*exp(5.73*z-5)+co_dr(:,4).*delta));
Illuminance_dr(Illuminance_dr<0)=0;

Illuminance_dh=round(Df.*(co_dh(:,1)+co_dh(:,2).*W+co_dh(:,3).*cos(z)+co_dh(:,4).*log(delta))); 

Luminance_z=round(Df.*(co_zl(:,1)+co_zl(:,2).*cos(z)+co_zl(:,3).*exp(-3*z)+co_zl(:,4).*delta));

for jj=1:length(Illuminance_g)
    if isnan(Illuminance_g(jj))==1;  Illuminance_g(jj)=0;
    end
    
    if isnan(Illuminance_dr(jj))==1;  Illuminance_dr(jj)=0;
    end
    
    if isnan(Illuminance_dh(jj))==1;  Illuminance_dh(jj)=0;
    end
    
    if isnan(Luminance_z(jj))==1;  Luminance_z(jj)=0;
    end
              
end
end










