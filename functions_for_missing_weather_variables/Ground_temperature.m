function Ground_T=Ground_temperature(TRY_T_S)
% Purpose: 
%        To calculate ground temperature from UKCP09 WG outputs 
% Define variables:
%
%               gt_m      -- Future montly ground temperature (degree C)
%               gt_d      -- Future daily ground temperature (degree C)

%               dbt_a     -- Future annual mean dry bulb temperature (degree C)
%               dbt_m     -- Future monthly mean dry bulb temperature (degree C)

%               dbt_amp   -- Amplitude of the warmest and coldest future
%               average monthly dry bulb temperature (degree C)
%
%               depth     -- Depth below surface
%               Ds        -- Thermal diffusivity of the ground/soil (m2/day)   
%               d         -- Day of the year  
%               d_shift   -- Median day of the month containing the minimum suface temperature
%                            (values: 15, 46, 74, 95, 135, 166, 196, 227, 258, 288, 319, 349)


dbt_m=zeros(12,1);
gt_m=zeros(12,1);

% depth=[0.5,2,4]; 
Ds=0.055741824; % Default value given by Lawrie
d=TRY_T_S(:,4);
d_shift=[15, 46, 74, 95, 135, 166, 196, 227, 258, 288, 319, 349];
dbt_a=mean(TRY_T_S(:,6));

month=TRY_T_S(:,2);
for pp=1:12
%     n=find(month==pp); 
    dbt_m(pp,1)=mean(TRY_T_S(month==pp,6));
    d_shift_d(month==pp)=d_shift(pp); % copied median day values to daily values
end

dbt_amp=max(dbt_m)-min(dbt_m);

% Ground Temperature
x=sqrt(pi/(Ds*365))*2; % depth=[0.5,2,4]; depth=2
y=(exp(-x)^2-2*exp(-x)*cos(x)+1)/2*x^2;
z=(1-exp(-x)*(cos(x)+sin(x)))/(1-exp(-x)*(cos(x)-sin(x)));

gt_d=dbt_a-dbt_amp*cos(2*(pi/365)*d-(d_shift_d'.*0.017214+0.341787)-atan(z))*sqrt(y);

for qq=1:12
%     m=find(month==qq); 
    gt_m(qq,1)=mean(gt_d(month==qq));   % convert to monthly mean values
    
end
Ground_T=gt_m';
end


