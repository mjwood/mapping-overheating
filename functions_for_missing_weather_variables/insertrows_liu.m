function new=insertrows_liu(mat1, mat2,r)
% insert rows before the r
[rows, ~]=size(mat2);
new1(1:r-1,:)=mat1(1:r-1,:); new1(r:r+rows-1,:)=mat2;

mat1(1:r-1,:)=[]; new2=mat1;

new=[new1;new2];



% mat1=zeros(5);
% 
% 
% mat2=[1 1 1 1 1];
% 
% r=3;
% 
% new=insertrows_liu(mat1, mat2,r)