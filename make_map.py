# this is based on the tutorial you can find here:
# http://www.acgeospatial.co.uk/geopandas-shapefiles-jupyter/
# Good source of shapefiles:
# http://www.diva-gis.org/gdata
import matplotlib.pyplot as plt
import pandas as pd
import geopandas as gpd
import numpy as np
import os

# Bring the shape file
os.getcwd()
gdf = gpd.read_file('mapping/GBR_adm/GBR_adm2.shp') # admin areas by lat and long
gdf.columns

# read in the weather station data:
ws_df = pd.read_csv('WS_id_lat_lon_alt.csv', header = None)
ws_df.columns = ['ID','Lat','Long','Alt']

# create the plot of the shape file
ax = gdf.plot(figsize=(20, 10), color = "green", alpha = 0.5)
ax.set_title("A plot of the UK")
ax.plot()
ax.set_xlim( -10, 2 )
ax.scatter(ws_df['Long'], ws_df['Lat'], c=ws_df['Alt'], cmap = plt.cm.coolwarm )
plt.show()
