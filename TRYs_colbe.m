function TRY_Y=TRYs_colbe(hourly_data,lon,lat,WS_id, A)
%                        daily_data,
% FS: Temperature + Solar Radiation + Wind Speed

% Define variables:
%         hourly_data        -- UKCP09 weather generator hourly data
%         VP                 -- Vapour pressure 

%         Alt                -- Altitude
%         El                 -- Solar elevation 

%         T_dewpoint         -- Dew point temperature
%         RH                 -- Relative humidity (%)
%         Atmospheric_P      -- Atmospheric pressure (pasca)

%         I_hr   -- Extra-terrestrial horizontal radiance (Wh�m�2)
%         I_dr   -- Extra-terrestrial direct radiance (Wh�m�2)

%         DiffuseHorizontalR -- Diffuse horizontal radiation (W�m�2)
%         DirectNormalR      -- Direct normal radiation (W�m�2)
%         GIRad              -- Global irradiance for any given cloud amount (W�m�2)

%         L_sky   -- Horizontal Infrared Radiation from Sky (W�m�2)

%         Illuminance_g      -- Global illuminance on the horizontal plane (lux)
%         Illuminance_dh     -- Diffuse illuminance on the horizontal plane (lux)
%         Illuminance_dr     -- Direct normal illuminance (lux)
%         Luminance_z        -- Zenith luminance (cd/m2)

%         Wind_D             -- Wind direction (degree)
%         Wind_S             -- Wind speed (m/s)
%         filestem           -- Observed weather data in 2013 from MIDAS

%         ts_cover           -- Total sky cover (deka)
%         os_cover           -- Opaque sky cover
%         Visibility         -- Visibility
%         Ceiling_H          -- Ceiling height

%         Weather_obsv       -- Present weather observation
%         Weather_code       -- Present weather code

%         PW                 -- Precipitation water (mm/hour)

%         Optical_depth      -- Aerosol optical depth
%         Snow_depth         -- Snow depth
%         Last_snow_fall     -- Days since last snow fall

% Solar elevation 
Alt=10;
date(:,1)=hourly_data(:,1)-1000; % SolarAzEl_liu can only recognise year under 3000
date(:,2)=hourly_data(:,2);
date(:,3)=hourly_data(:,3);
date(:,4)=hourly_data(:,4)/100;

% poolobj = gcp;
% addAttachedFiles(poolobj,{'SolarAzEl_liu.m','dew_point_temperature.m',...
%     'Extra_terrestrial_SolarR.m','Solar_Radiation.m','Horizontal_Infrared_Radiation.m',...
%     'Illuminance.m','WD_WS_AP_from_MIDAS_colbe.m','insertrows_liu.m'})

[~, El]= SolarAzEl_liu(date,lat,lon,Alt);
%%
% addpath('C:\Chunde Liu 2013\MATLAB codes for PRYs\functions for missing weather variables');

% PathFunction=genpath('functions_for_missing_weather_variables');
% addpath(PathFunction);

% 1 Dew point temperature
T_dewpoint=dew_point_temperature(hourly_data);
% 2 Sky Cover          ts_cover(data_indice),os_cover(data_indice),
sf=hourly_data(:,10);
ts_cover=10*(1-sf);
os_cover=round(0.5*ts_cover);
% 3 Precipitation
PW=hourly_data(:,5);
% 4 Relative Humidity
RH=hourly_data(:,8).*100;
% 5 Extraterrestrial Solar Radiance 
[I_hr,I_dr]= Extra_terrestrial_SolarR(El);
% 6 Solar Radiation Calculation
[GIRad,DiffuseHorizontalR,DirectNormalR]=Solar_Radiation(sf,El);
% 7 Horizontal Infrared Radiation from Sky
L_sky= Horizontal_Infrared_Radiation(hourly_data);
% 8 Illuminance and Zenith luminance
[Illuminance_g,Illuminance_dr,Illuminance_dh,Luminance_z]= Illuminance(hourly_data,I_dr,...
                                 GIRad,DiffuseHorizontalR,DirectNormalR,El);
% 9 Wind Direction, Speed and Atmospheric Pressure

[Wind_D,Wind_S,Atmospheric_P]=WD_WS_AP_from_MIDAS_colbe(hourly_data,WS_id, A);

%%
year=hourly_data(:,1);
month=hourly_data(:,2);
day=hourly_data(:,3);

TRY_Y=zeros(1,31);

for mm=1:12    
   Daylength=[31,28,31,30,31,30,31,31,30,31,30,31]; % month length
   u_T=[]; u_SR=[]; u_WS=[]; d_T=[]; d_SR=[]; d_WS=[];
    
   for dd=1:Daylength(mm)    % day   
       for y=1:30 % y=1:100
              index_md=find(month==mm & day==dd & (year==3000+y));        
              TSR_md=[hourly_data(index_md,1:6), GIRad(index_md,1), Wind_S(index_md,1) ];                

                u_T(y,dd)=mean(TSR_md(:,6));                d_T(y,dd)=std(double(TSR_md(:,6)));               TN(:,y)=TSR_md(:,6);            
                u_SR(y,dd)=mean(TSR_md(TSR_md(:,7)~=0,7));  d_SR(y,dd)=std(double(TSR_md(TSR_md(:,7)~=0,7))); SRN(:,y)=TSR_md(:,7);  
                u_WS(y,dd)=mean(TSR_md(:,8));               d_WS(y,dd)=std(double(TSR_md(:,8)));              WSN(:,y)=TSR_md(:,8);                  
       end       
       
            U_T(mm,dd)=mean(u_T(:,dd));         D_T(mm,dd)=std(double(reshape(TN,[],1)));
            U_SR(mm,dd)=mean(u_SR(:,dd));       D_SR(mm,dd)=std(double(reshape(SRN,[],1)));
            U_WS(mm,dd)=mean(u_WS(:,dd));       D_WS(mm,dd)=std(double(reshape(WSN,[],1)));        
   end
            u_T_month{mm}=u_T;    d_T_month{mm}=d_T;
            u_SR_month{mm}=u_SR;  d_SR_month{mm}=d_SR;
            u_WS_month{mm}=u_WS;  d_WS_month{mm}=d_WS;
end

% FS statistics    
     x1=-20:50; % T
     x2=0:1000; % SR
     x3=0:20;% WS   
            
for mmm=1:12   % mmm=1:12  
%     length=[31,28,31,30,31,30,31,31,30,31,30,31]; % month length
    for ddd=1:Daylength(mmm)  
        for yyy=1:30
            % Temperature
            FS1(yyy,1)=sum(abs(cdf('Normal',x1,u_T_month{mmm}(yyy,ddd),d_T_month{mmm}(yyy,ddd))-cdf('Normal',x1,U_T(mmm,ddd),D_T(mmm,ddd))));  
            % SR
            FS2(yyy,1)=sum(abs(cdf('Normal',x2,u_SR_month{mmm}(yyy,ddd),d_SR_month{mmm}(yyy,ddd))-cdf('Normal',x2,U_SR(mmm,ddd),D_SR(mmm,ddd))));
            % WS
            FS3(yyy,1)=sum(abs(cdf('Normal',x3,u_WS_month{mmm}(yyy,ddd),d_WS_month{mmm}(yyy,ddd))-cdf('Normal',x3,U_WS(mmm,ddd),D_WS(mmm,ddd))));
        end
    end   
FS=(FS1+FS2+FS3)./3;   [~,I] = min(FS); 
  
%    TRY_year=3000+I;
   data_indice= find((month==mmm) & (year==3000+I));
   y=hourly_data(data_indice,1);
   m=hourly_data(data_indice,2);
   day=hourly_data(data_indice,3);
   h=hourly_data(data_indice,4);
   minute=hourly_data(data_indice,5);
% Unavailable weather variables
   Visibility=9999+zeros(length(h),1);
   Ceiling_H=99999+zeros(length(h),1);
   Weather_obsv=9+zeros(length(h),1);
   Weather_code=999999999+zeros(length(h),1);
   Optical_depth=999+zeros(length(h),1);
   Snow_depth=999+zeros(length(h),1);
   Last_snow_fall=99+zeros(length(h),1);
   
  TRY_M=[y,m,day,h,minute,hourly_data(data_indice,6),T_dewpoint(data_indice),...
       RH(data_indice),Atmospheric_P(data_indice),I_hr(data_indice),I_dr(data_indice),L_sky(data_indice),...
       GIRad(data_indice),DirectNormalR(data_indice),DiffuseHorizontalR(data_indice),...
       Illuminance_g(data_indice),Illuminance_dr(data_indice),Illuminance_dh(data_indice),...
       Luminance_z(data_indice),Wind_D(data_indice),Wind_S(data_indice),ts_cover(data_indice),os_cover(data_indice),...
       Visibility,Ceiling_H,Weather_obsv,Weather_code,PW(data_indice),Optical_depth,Snow_depth,Last_snow_fall];  
  TRY_Y=vertcat(TRY_Y, TRY_M);
   
end
 TRY_Y(1,:)=[];

 
 
 
 
end





